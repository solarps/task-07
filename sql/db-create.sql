DROP database IF EXISTS test2db;

CREATE database test2db;

CREATE TABLE users (
	id serial PRIMARY KEY,
	login VARCHAR(10)
);

CREATE TABLE teams (
	id serial PRIMARY KEY ,
	name VARCHAR(10)
);

create table users_teams
(
    user_id int
        constraint users_id_fk
            references users(id) ON DELETE CASCADE ,
    teams_id int
        constraint teams_id_fk
            references teams(id) ON DELETE CASCADE
);


INSERT INTO users VALUES (DEFAULT, 'ivanov');
INSERT INTO teams VALUES (DEFAULT, 'teamA');

SELECT * FROM users;
SELECT * FROM teams;
SELECT * FROM users_teams;

